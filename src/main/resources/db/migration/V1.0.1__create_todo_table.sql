CREATE TABLE todo (
    id BIGINT NOT NULL DEFAULT nextval('hibernate_sequence'),
    complete BOOLEAN NOT NULL DEFAULT false,
    created TIMESTAMPTZ NOT NULL DEFAULT now(),
    description TEXT,
    duedate TIMESTAMPTZ,
    title VARCHAR(255),
    PRIMARY KEY (id)
);
