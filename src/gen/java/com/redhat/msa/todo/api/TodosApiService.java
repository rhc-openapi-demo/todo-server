package com.redhat.msa.todo.api;

import com.redhat.msa.todo.api.*;
import com.redhat.msa.todo.models.*;


import com.redhat.msa.todo.models.Todo;

import java.util.List;
import com.redhat.msa.todo.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaResteasyServerCodegen", date = "2020-01-19T15:13:58.486-05:00[America/Kentucky/Louisville]")
public interface TodosApiService {
      Response createTodo(Todo todo,SecurityContext securityContext)
      throws NotFoundException;
      Response deleteTodo(Long todoId,SecurityContext securityContext)
      throws NotFoundException;
      Response getTodo(Long todoId,SecurityContext securityContext)
      throws NotFoundException;
      Response gettodos(SecurityContext securityContext)
      throws NotFoundException;
      Response updateTodo(Long todoId,Todo todo,SecurityContext securityContext)
      throws NotFoundException;
}
