package com.redhat.msa.todo.api;

import com.redhat.msa.todo.models.*;
import com.redhat.msa.todo.api.TodosApiService;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import com.redhat.msa.todo.models.Todo;

import java.util.Map;
import java.util.List;
import com.redhat.msa.todo.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;
import javax.inject.Inject;

import javax.validation.constraints.*;
import javax.validation.Valid;

@Path("/todos")


@io.swagger.annotations.Api(description = "the todos API")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaResteasyServerCodegen", date = "2020-01-19T15:13:58.486-05:00[America/Kentucky/Louisville]")
public class TodosApi  {

    @Inject TodosApiService service;

    @POST
    
    @Consumes({ "application/json" })
    
    @io.swagger.annotations.ApiOperation(value = "Create a Todo", notes = "Creates a new instance of a `Todo`.", response = Void.class, tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 201, message = "Successful response.", response = Void.class) })
    public Response createTodo(@ApiParam(value = "A new `Todo` to be created." ,required=true) @NotNull @Valid Todo todo,@Context SecurityContext securityContext)
    throws NotFoundException {
        return service.createTodo(todo,securityContext);
    }
    @DELETE
    @Path("/{todoId}")
    
    
    @io.swagger.annotations.ApiOperation(value = "Delete a Todo", notes = "Deletes an existing `Todo`.", response = Void.class, tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 204, message = "Successful response.", response = Void.class) })
    public Response deleteTodo( @PathParam("todoId") Long todoId,@Context SecurityContext securityContext)
    throws NotFoundException {
        return service.deleteTodo(todoId,securityContext);
    }
    @GET
    @Path("/{todoId}")
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "Get a Todo", notes = "Gets the details of a single instance of a `Todo`.", response = Todo.class, tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 200, message = "Successful response - returns a single `Todo`.", response = Todo.class) })
    public Response getTodo( @PathParam("todoId") Long todoId,@Context SecurityContext securityContext)
    throws NotFoundException {
        return service.getTodo(todoId,securityContext);
    }
    @GET
    
    
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "List All todos", notes = "Gets a list of all `Todo` entities.", response = Todo.class, responseContainer = "List", tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 200, message = "Successful response - returns an array of `Todo` entities.", response = Todo.class, responseContainer = "List") })
    public Response gettodos(@Context SecurityContext securityContext)
    throws NotFoundException {
        return service.gettodos(securityContext);
    }
    @PUT
    @Path("/{todoId}")
    @Consumes({ "application/json" })
    
    @io.swagger.annotations.ApiOperation(value = "Update a Todo", notes = "Updates an existing `Todo`.", response = Void.class, tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 202, message = "Successful response.", response = Void.class) })
    public Response updateTodo( @PathParam("todoId") Long todoId,@ApiParam(value = "Updated `Todo` information." ,required=true) @NotNull @Valid Todo todo,@Context SecurityContext securityContext)
    throws NotFoundException {
        return service.updateTodo(todoId,todo,securityContext);
    }
}
