library identifier: "pipeline-library@v1.4",
        retriever: modernSCM(
                [
                        $class: "GitSCMSource",
                        remote: "https://github.com/redhat-cop/pipeline-library.git"
                ]
        )

openshift.withCluster() {
  env.NAMESPACE = openshift.project()
  env.APP_NAME = 'todo-server'
  env.UPSTREAM = JOB_NAME.replaceAll(/todo-server/, 'jpa-entities')
  echo "Starting Pipeline for ${APP_NAME}..."
  env.BUILD = "${env.NAMESPACE}"
  env.DEV = env.NAMESPACE.replaceAll('-ci-cd$', '-dev')
  env.TEST = env.NAMESPACE.replaceAll('-ci-cd$', '-test')
}

pipeline {
  // Use Jenkins Maven slave
  // Jenkins will dynamically provision this as OpenShift Pod
  // All the stages and steps of this Pipeline will be executed on this Pod
  // After Pipeline completes the Pod is killed so every run will have clean
  // workspace
  agent {
    label 'jenkins-slave-graal'
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '5'))
  }

  triggers {
    upstream(
            upstreamProjects: env.UPSTREAM,
            threshold: hudson.model.Result.SUCCESS
    )
  }

  // Pipeline Stages start here
  // Requeres at least one stage
  stages {
    // Run Maven build, skipping tests
    stage('Build'){
      steps {
        sh "./mvnw -B clean install -DskipTests=true"
      }
    }

    // Run Maven unit tests
    stage('Unit Test'){
      steps {
        sh "./mvnw -B test"
      }
    }

    // Build Container Image using the artifacts produced in previous stages
    stage('Build Container Image'){
      steps {
        // Copy the resulting artifacts into common directory
        sh """
          rm -rf oc-build && mkdir -p oc-build/deployments
          cp -a target/lib oc-build/deployments/
          cp -a target/*runner.jar oc-build/deployments/
        """

        // Build container image using local Openshift cluster
        // Giving all the artifacts to OpenShift Binary Build
        // This places your artifacts into right location inside your S2I image
        // if the S2I image supports it.
        binaryBuild(projectName: env.BUILD, buildConfigName: env.APP_NAME, buildFromPath: "oc-build")
      }
    }

    stage('Promote from Build to Dev') {
      steps {
        tagImage(sourceImageName: env.APP_NAME, sourceImagePath: env.BUILD, toImagePath: env.DEV)
      }
    }

    stage ('Verify Deployment to Dev') {
      steps {
        verifyDeployment(projectName: env.DEV, targetApp: env.APP_NAME)
      }
    }

    stage('Promote from Dev to Test') {
      steps {
        tagImage(sourceImageName: env.APP_NAME, sourceImagePath: env.DEV, toImagePath: env.TEST)
      }
    }

    stage ('Verify Deployment to Test') {
      steps {
        verifyDeployment(projectName: env.TEST, targetApp: env.APP_NAME)
      }
    }
  }
}